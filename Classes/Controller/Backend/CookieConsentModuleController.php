<?php
declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace HauerHeinrich\HhExtCookieConsent\Controller\Backend;

use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;
use \TYPO3\CMS\Backend\Attribute\Controller;
use \TYPO3\CMS\Backend\Template\ModuleTemplateFactory;
// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use \TYPO3\CMS\Extbase\Http\ForwardResponse;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Backend\Utility\BackendUtility;
use \TYPO3\CMS\Backend\Routing\UriBuilder;
use \HauerHeinrich\HhExtCookieConsent\Domain\Repository\CookieconsentRepository;

/***
 *
 * This file is part of the "hh_ext_cookie_consent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2023
 *
 ***/
#[Controller]
class CookieConsentModuleController extends ActionController {

    protected string $extensionKey = '';
    protected array $extensionConfiguration = [];
    protected int $selectedPageId = 0;
    protected int $rootPageId = 0;

    /**
     * Cookie Object Element
     */
    protected $cookieObjectElement = [];

    public function __construct(
        protected readonly ModuleTemplateFactory $moduleTemplateFactory,
        protected CookieconsentRepository $cookieconsentRepository
    ) {
    }

    protected function init(ServerRequestInterface $request): void {
        $this->extensionKey = 'hh_ext_cookie_consent';
        // Typo3 extension manager gearwheel icon (ext_conf_template.txt)
        $this->extensionConfiguration = isset($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$this->extensionKey]) ? $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'][$this->extensionKey] : [];

        // Get current selected page from pageTree
        $site = $request->getAttribute('site', 0);
        if($site !== 0) {
            $this->selectedPageId = $site->getRootPageId();
        }

        foreach (array_reverse(BackendUtility::BEgetRootLine($this->selectedPageId)) as $value) {
            if($value['is_siteroot'] === 1) {
                $this->rootPageId = $value['uid'];
                break;
            }
        }

        $this->cookieObjectElement = $this->cookieconsentRepository->findOneByPid($this->rootPageId);
    }

    /**
     * Return simple dummy content
     */
    public function indexAction(ServerRequestInterface $request): ResponseInterface {
        $view = $this->moduleTemplateFactory->create($request);
        $init = $this->init($request);

        if($this->checkIfPageIsSelected() === false) {
            return $view->renderResponse('Backend/CookieConsentModule/noPageSelected');
        }

        // if current siteConfig / domain has no DB record
        if ($this->cookieObjectElement == null) {
            $cookieObj = GeneralUtility::makeInstance('HauerHeinrich\\HhExtCookieConsent\\Domain\\Model\\Cookieconsent');
            $cookieObj->setPid($this->rootPageId);
            $this->cookieconsentRepository->add($cookieObj);
            GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class)->persistAll();
            $this->cookieObjectElement = $cookieObj;
        }

        // Redirect
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $uri = (string)$uriBuilder->buildUriFromRoute(
            'record_edit',
            [
                'edit[tx_hhextcookieconsent_domain_model_cookieconsent][' . $this->cookieObjectElement->getUid() . ']' => 'edit',
                'returnUrl' => GeneralUtility::getIndpEnv('REQUEST_URI')
            ]
        );

        return $this->responseFactory->createResponse(200)
            ->withHeader('Location', $uri);
    }

    /**
     * action cookieSelection
     *
     * @return ResponseInterface
     */
    public function cookieSelectionAction(ServerRequestInterface $request): ResponseInterface {
        $view = $this->moduleTemplateFactory->create($request);
        $init = $this->init($request);

        if($this->checkIfPageIsSelected() === false) {
            return $view->renderResponse('Backend/CookieConsentModule/noPageSelected');
        }

        // if current siteConfig / domain has no DB record
        if (empty($this->cookieObjectElement)) {
            $cookieObject = GeneralUtility::makeInstance('HauerHeinrich\\HhExtCookieConsent\\Domain\\Model\\Cookieconsent');
            $cookieObject->setPid($this->rootPageId);
            $this->cookieconsentRepository->add($cookieObject);
        } else {
            $cookieObject = $this->cookieObjectElement;
        }

        GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class)->persistAll();

        $view->setTitle('Cookie Selection');
        $view->getDocHeaderComponent()->disable();

        $view->assignMultiple([
            'menuIndex' => 'cookieSelection',
            'cookieObject' => $cookieObject,
        ]);

        return $view->renderResponse('Backend/CookieConsentModule/CookieSelection');
    }

    /**
     * action noPageSelected
     *
     * @return ResponseInterface
     */
    public function noPageSelectedAction(ServerRequestInterface $request): ResponseInterface {
        $view = $this->moduleTemplateFactory->create($request);
        $init = $this->init($request);

        $view->setTitle('No Page Selected');
        $view->getDocHeaderComponent()->disable();

        $view->assignMultiple([
            'menuIndex' => 'noPageSelected',
        ]);

        return $view->renderResponse('Backend/CookieConsentModule/NoPageSelected');
    }

    /**
     * checkIfPageIsSelected - forward/redirect to other action if no page is selected
     *
     * @return bool
     */
    public function checkIfPageIsSelected(): bool {
        // if no page is selected || if rootpage id=0 is selected
        if(empty($this->selectedPageId) || $this->selectedPageId == 0) {
            return false;
        }

        return true;
    }

    /**
     * getModuleMenuInfo
     * returns a array with a url and a label for the url to build a link like a html a-tag (<a href="...)
     *
     * @return array
     */
    public function getModuleMenuInfo(): array {
        // get uri's for ModuleMenue
        // tx_hhextcookieconsent_web_hhextcookieconsentcookieconsentmodule:
        // tx_ + extensionkey + _backendcategory_ + Backend->Configuration->Backendrouten name
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $uris = [];
        // indexAction
        $uris[0]['link'] = (string)$uriBuilder->buildUriFromRoute('cookieconsentmodule', ['id' => $this->selectedPageId]);
        $uris[0]['label'] = 'Index'; // e. g. \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_orders_domain_model_order.' . $key,'Orders')

        // cookieSelectionAction
        $uris[1]['link'] = (string)$uriBuilder->buildUriFromRoute('cookieconsentmodule.cookieSelection', ['id' => $this->selectedPageId]);
        $uris[1]['label'] = 'Cookie selection';

        return $uris;
    }
}
