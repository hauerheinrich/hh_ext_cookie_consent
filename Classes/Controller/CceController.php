<?php
namespace HauerHeinrich\HhExtCookieConsent\Controller;

use \Psr\Http\Message\ResponseInterface;
// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***
 *
 * This file is part of the "hh_ext_cookie_consent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

class CceController extends ActionController {

    protected string $extensionKey = 'hh_ext_cookie_consent';

    public function __construct() {
    }

    public function initializeView(): void {

        $this->view->assignMultiple([
            'settings' => $this->settings,
            'data' => $this->request->getAttribute('currentContentObject')->data
        ]);
    }

    public function consentIframeAction(): ResponseInterface {
        return $this->htmlResponse();
    }
}
