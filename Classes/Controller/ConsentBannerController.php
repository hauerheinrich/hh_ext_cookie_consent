<?php
namespace HauerHeinrich\HhExtCookieConsent\Controller;

use \Psr\Http\Message\ResponseInterface;
// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;
use \HauerHeinrich\HhExtCookieConsent\Domain\Repository\CookieconsentRepository;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\Cookieconsent;

/***
 *
 * This file is part of the "hh_ext_cookie_consent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/

class ConsentBannerController extends ActionController {

    protected string $extensionKey = 'hh_ext_cookie_consent';
    protected int $rootPageId = 0;
    protected ?Cookieconsent $cookieObject = null;

    public function __construct(protected CookieconsentRepository $cookieconsentRepository) {
        $this->rootPageId = $GLOBALS['TSFE']->rootLine[0]['uid'];
        $this->cookieObject = $this->cookieconsentRepository->findByPid($this->rootPageId)[0];
    }

    /**
     * Set up the doc header properly here
     *
     * @param $view
     */
    protected function initializeView($view): void {
        if(!empty($this->cookieObject)) {
            $this->cookieObject->setMarketingFields();
            $this->cookieObject->setOthersFields();
            $this->cookieObject->setMediaFields();
        }

        $this->view->assignMultiple([
            'rootPage' => $this->rootPageId,
            'cookieObject' => $this->cookieObject,
        ]);
    }

    /**
     * action show
     */
    public function bannerOutputAction(): ResponseInterface {
        $beConsentSettings = $this->cookieconsentRepository->findByPid($this->rootPageId);

        // Check backend "Enabled" field "hidden"
        if(isset($beConsentSettings[0])) {
            $this->view->assignMultiple([
                'hidden' => $beConsentSettings[0]->getHidden()
            ]);

            if($beConsentSettings[0]->getHidden() === 1) {
                return $this->htmlResponse();
            }
        } else {
            $this->view->assignMultiple([
                'hidden' => 1
            ]);

            return $this->htmlResponse();
        }

        $cookieContentObj = new Cookieconsent();

        // WARNING, this is the old value, you have to refresh to see the new / correct one!
        // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($_COOKIE['hh_ext_cookie_consent'], "bannerOutputAction hh_ext_cookie_consent: ");

        // START: special for custom fields
        $cookieArray = [];
        if(!empty($_COOKIE['hh_ext_cookie_consent'])) {
            $cookieArray = \json_decode($_COOKIE['hh_ext_cookie_consent'], true);
        }

        if(!empty($_POST) && isset($_POST['tx_hhextcookieconsent_coco'])) {
            if(array_key_exists('submit_all', $_POST['tx_hhextcookieconsent_coco']) && $_POST['tx_hhextcookieconsent_coco']['submit_all'] === '1') {
                if(empty($this->cookieObject)) {
                    $this->addFlashMessage(
                        'It seems that no settings have been made yet in the backend module of the Consent-banner.',
                        'Error',
                        ContextualFeedbackSeverity::ERROR,
                        false
                    );
                } else {
                    $customFields = $this->cookieObject->getCustomFields();
                    if(!empty($customFields)) {
                        $cookieArray = [];
                        foreach ($customFields as $customField) {
                            $cookieArray['custom'][$customField->getUid()] = 1;
                        }
                    }
                }
            }

            if(array_key_exists('submit_essentials', $_POST['tx_hhextcookieconsent_coco']) && $_POST['tx_hhextcookieconsent_coco']['submit_essentials'] === '1') {
                $cookieArray = [];
            }
        }
        // END: special for custom fields

        // TODO: improve this code about privacyRespectGpc
        $privacyRespectGpc = true;
        if(empty($this->cookieObject)) {
            $browserSettingPrivacyRespectGpc = 0;
            // GDPR respect user settings
            // Check browser-setting of user against DNT:
            if(isset($_SERVER['HTTP_DNT']) && $_SERVER['HTTP_DNT'] === '1') {
                $browserSettingPrivacyRespectGpc = 1;
            }

            // Check browser-setting of user against GPC:
            if(isset($_SERVER['HTTP_SEC_GPC']) && $_SERVER['HTTP_SEC_GPC'] === '1') {
                $browserSettingPrivacyRespectGpc = 1;
            }

            $this->addFlashMessage(
                'It seems that no settings have been made yet in the backend module of the Consent-banner.',
                'Error',
                ContextualFeedbackSeverity::ERROR,
                false
            );
        } else {
            // DoNotTrack setting Browser
            // https://walterebert.com/blog/respecting-browser-privacy-settings/
            // Check if we should respect user DoNotTrack-browser-setting
            $browserSettingPrivacyRespectGpc = 0;
            $privacyRespectGpc = $this->cookieObject->getPrivacyRespectGpc();
            if($privacyRespectGpc === 1) {
                // Check browser-setting of user against DNT:
                if(isset($_SERVER['HTTP_DNT']) && $_SERVER['HTTP_DNT'] === '1') {
                    $browserSettingPrivacyRespectGpc = 1;
                }

                // Check browser-setting of user against GPC:
                if(isset($_SERVER['HTTP_SEC_GPC']) && $_SERVER['HTTP_SEC_GPC'] === '1') {
                    $browserSettingPrivacyRespectGpc = 1;
                }
            }
        }

        $this->view->assignMultiple([
            'menuIndex' => 'bannerOutput',
            'cookieObjectNew' => $cookieContentObj,
            'oldUserCookieSettings' => $cookieArray,
            'privacyRespectGpc' => $browserSettingPrivacyRespectGpc,
        ]);

        return $this->htmlResponse();
    }
}
