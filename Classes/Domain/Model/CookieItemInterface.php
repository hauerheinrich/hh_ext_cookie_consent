<?php
namespace HauerHeinrich\HhExtCookieConsent\Domain\Model;

interface CookieItemInterface {
    public function getTitle(): string;

    public function getCookieId(): string;
}
