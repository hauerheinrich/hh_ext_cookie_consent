<?php
namespace HauerHeinrich\HhExtCookieConsent\Domain\Model;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\Essentials;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\Custom;

/***
 *
 * This file is part of the "Cookie Consent Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Christian Hackl <chackl@hauer-heinrich.de>, Hauer Heinrich
 *           Sebastian Pieczona <spieczona@hauer-heinrich.de>, Hauer-Heinrich
 *
 ***/

/**
 * Cookieconsent
 */
class Cookieconsent extends AbstractEntity {

    protected ?\DateTime $crdate;
    protected ?\DateTime $tstamp;
    protected int $sysLanguageUid;
    protected int $l10nParent;
    protected ?\DateTime $starttime;
    protected ?\DateTime $endtime;
    protected int $hidden;
    protected int $deleted;
    protected int $cruserId;
    protected int $sorting = 0;

    protected string $cookiePrivacyLink = '';
    protected string $cookieImprintLink = '';
    protected string $cookieName = '';
    protected string $cookieVendor = '';
    protected int $cookieOverlaySettingsButton = 0;
    protected string $cookieThemeColor = '';
    protected string $cookieTheme = '';
    protected string $cookieCssPath = '';
    protected string $cookieEssentialsHeader = '';
    protected string $cookieEssentialsDescription = '';
    protected int $privacyRespectGpc = 0;
    protected int $cookieFooterSettingsButton = 0;
    protected int $cookieBannerCloseIcon = 0;
    protected int $cookieBannerBtnEssentialShow = 0;
    protected int $googleAnalytics = 0;
    protected int $googleTagManager = 0;
    protected int $googleAdsRemarketing = 0;
    protected int $matomo = 0;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Validate("Url")
     */
    protected string $matomoUrl = '';

    protected int $plausible = 0;

    /**
     * @TYPO3\CMS\Extbase\Annotation\Validate("Url")
     */
    protected string $plausibleUrl = '';

    protected int $youtube = 0;
    protected int $stripe = 0;
    protected int $googleMaps = 0;
    protected int $openstreetmap = 0;
    protected int $googleAds = 0;
    protected int $vimeo = 0;

    /**
     * essentialsFields
     *
     * @var ObjectStorage<Essentials>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected ?ObjectStorage $essentialsFields = null;

    /**
     * customFields
     *
     * @var ObjectStorage<Custom>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected ?ObjectStorage $customFields = null;

    protected array $marketingFields = [];
    protected array $othersFields = [];
    protected array $mediaFields = [];

    /**
     * __construct
     */
    public function __construct()
    {
        // Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects(): void
    {
        $this->essentialsFields = new ObjectStorage();
        $this->customFields = new ObjectStorage();
    }

    public function getCrdate(): ?\DateTime
    {
        return $this->crdate;
    }

    public function setCrdate(\DateTime $crdate): void
    {
        $this->crdate = $crdate;
    }

    public function getYearOfCrdate(): int
    {
        return $this->getCrdate()->format('Y');
    }

    public function getMonthOfCrdate(): int
    {
        return $this->getCrdate()->format('m');
    }

    public function getDayOfCrdate(): int
    {
        return (int)$this->crdate->format('d');
    }

    public function getTstamp(): ?\DateTime
    {
        return $this->tstamp;
    }

    public function setTstamp(\DateTime $tstamp): void
    {
        $this->tstamp = $tstamp;
    }

    public function setSysLanguageUid(int $sysLanguageUid): void
    {
        $this->_languageUid = $sysLanguageUid;
    }

    public function getSysLanguageUid(): int
    {
        return $this->_languageUid;
    }

    public function setL10nParent(int $l10nParent): void
    {
        $this->l10nParent = $l10nParent;
    }

    public function getL10nParent(): int
    {
        return $this->l10nParent;
    }

    public function getYearOfTstamp(): int
    {
        return $this->getTstamp()->format('Y');
    }

    public function getMonthOfTstamp(): int
    {
        return $this->getTstamp()->format('m');
    }

    public function getDayOfTimestamp(): int
    {
        return (int)$this->tstamp->format('d');
    }

    public function getCruserId(): int
    {
       return $this->cruserId;
    }

    public function setCruserId(int $cruserId): void
    {
       $this->cruserId = $cruserId;
    }

    public function getHidden(): int
    {
        return $this->hidden;
    }

    public function setHidden(int $hidden): void
    {
        $this->hidden = $hidden;
    }

    public function getDeleted(): int
    {
        return $this->deleted;
    }

    public function setDeleted(int $deleted): void
    {
        $this->deleted = $deleted;
    }

    public function getStarttime(): ?\DateTime
    {
        return $this->starttime;
    }

    public function setStarttime(\DateTime $starttime): void
    {
        $this->starttime = $starttime;
    }

    public function getYearOfStarttime(): int
    {
        if ($this->getStarttime()) {
            return $this->getStarttime()->format('Y');
        }
    }

    public function getMonthOfStarttime(): int
    {
        if ($this->getStarttime()) {
            return $this->getStarttime()->format('m');
        }
    }

    public function getDayOfStarttime(): int
    {
        if ($this->starttime) {
            return (int)$this->starttime->format('d');
        }
    }

    public function getEndtime(): ?\DateTime
    {
        return $this->endtime;
    }

    public function setEndtime(\DateTime $endtime): void
    {
        $this->endtime = $endtime;
    }

    public function getYearOfEndtime(): int
    {
        if ($this->getEndtime()) {
            return $this->getEndtime()->format('Y');
        }
    }

    public function getMonthOfEndtime(): int
    {
        if ($this->getEndtime()) {
            return $this->getEndtime()->format('m');
        }
    }

    public function getDayOfEndtime(): int
    {
        if ($this->endtime) {
            return (int)$this->endtime->format('d');
        }
    }

    public function getSorting(): int
    {
        return $this->sorting;
    }

    public function setSorting(int $sorting): void
    {
        $this->sorting = $sorting;
    }

    public function getCookiePrivacyLink(): string
    {
        return $this->cookiePrivacyLink;
    }

    public function setCookiePrivacyLink(string $cookiePrivacyLink): void
    {
        $this->cookiePrivacyLink = $cookiePrivacyLink;
    }

    public function getCookieImprintLink(): string
    {
        return $this->cookieImprintLink;
    }

    public function setCookieImprintLink(string $cookieImprintLink): void
    {
        $this->cookieImprintLink = $cookieImprintLink;
    }

    public function getCookieName(): string
    {
        return $this->cookieName;
    }

    public function setCookieName(string $cookieName): void
    {
        $this->cookieName = $cookieName;
    }

    public function getCookieVendor(): string
    {
        return $this->cookieVendor;
    }

    public function setCookieVendor(string $cookieVendor): void
    {
        $this->cookieVendor = $cookieVendor;
    }

    public function getCookieThemeColor(): string
    {
        return $this->cookieThemeColor;
    }

    public function setCookieThemeColor(string $cookieThemeColor): void
    {
        $this->cookieThemeColor = $cookieThemeColor;
    }

    public function getCookieTheme(): string
    {
        return $this->cookieTheme;
    }

    public function setCookieTheme(string $cookieTheme): void
    {
        $this->cookieTheme = $cookieTheme;
    }

    public function getCookieCssPath(): string
    {
        return $this->cookieCssPath;
    }

    public function setCookieCssPath(string $cookieCssPath): void
    {
        $this->cookieCssPath = $cookieCssPath;
    }

    public function getCookieEssentialsHeader(): string
    {
        return $this->cookieEssentialsHeader;
    }

    public function setCookieEssentialsHeader(string $cookieEssentialsHeader): void
    {
        $this->cookieEssentialsHeader = $cookieEssentialsHeader;
    }

    public function getCookieEssentialsDescription(): string
    {
        return $this->cookieEssentialsDescription;
    }

    public function setCookieEssentialsDescription(string $cookieEssentialsDescription): void
    {
        $this->cookieEssentialsDescription = $cookieEssentialsDescription;
    }

    public function getPrivacyRespectGpc(): int
    {
        return $this->privacyRespectGpc;
    }

    public function setPrivacyRespectGpc(int $privacyRespectGpc): void
    {
        $this->privacyRespectGpc = $privacyRespectGpc;
    }

    public function getCookieOverlaySettingsButton(): int
    {
        return $this->cookieOverlaySettingsButton;
    }

    public function setCookieOverlaySettingsButton(int $cookieOverlaySettingsButton): void
    {
        $this->cookieOverlaySettingsButton = $cookieOverlaySettingsButton;
    }

    public function getCookieFooterSettingsButton(): int
    {
        return $this->cookieFooterSettingsButton;
    }

    public function setCookieFooterSettingsButton(int $cookieFooterSettingsButton): void
    {
        $this->cookieFooterSettingsButton = $cookieFooterSettingsButton;
    }

    public function getCookieBannerCloseIcon(): int
    {
        return $this->cookieBannerCloseIcon;
    }

    public function setCookieBannerCloseIcon(int $cookieBannerCloseIcon): void
    {
        $this->cookieBannerCloseIcon = $cookieBannerCloseIcon;
    }

    public function getCookieBannerBtnEssentialShow(): int
    {
        return $this->cookieBannerBtnEssentialShow;
    }

    public function setCookieBannerBtnEssentialShow(int $cookieBannerBtnEssentialShow): void
    {
        $this->cookieBannerBtnEssentialShow = $cookieBannerBtnEssentialShow;
    }

    public function setGoogleAnalytics(int $googleAnalytics): void
    {
        $this->googleAnalytics = $googleAnalytics;
    }

    public function getGoogleAnalytics(): int
    {
        return $this->googleAnalytics;
    }

    public function getGoogleTagManager(): int
    {
        return $this->googleTagManager;
    }

    public function setGoogleTagManager(int $googleTagManager): void
    {
        $this->googleTagManager = $googleTagManager;
    }

    public function getGoogleAdsRemarketing(): int
    {
        return $this->googleAdsRemarketing;
    }

    public function setGoogleAdsRemarketing(int $googleAdsRemarketing): void
    {
        $this->googleAdsRemarketing = $googleAdsRemarketing;
    }

    public function getMatomo(): int
    {
        return $this->matomo;
    }

    public function setMatomo(int $matomo): void
    {
        $this->matomo = $matomo;
    }

    public function getMatomoUrl(): string
    {
        return $this->matomoUrl;
    }

    public function setMatomoUrl(string $matomoUrl): void
    {
        $this->matomoUrl = $matomoUrl;
    }

    public function getPlausible(): int
    {
        return $this->plausible;
    }

    public function setPlausible(int $plausible): void
    {
        $this->plausible = $plausible;
    }

    public function getPlausibleUrl(): string
    {
        return $this->plausibleUrl;
    }

    public function setPlausibleUrl(string $plausibleUrl): void
    {
        $this->plausibleUrl = $plausibleUrl;
    }

    public function getYoutube(): int
    {
        return $this->youtube;
    }

    public function setYoutube(int $youtube): void
    {
        $this->youtube = $youtube;
    }

    public function getStripe(): int
    {
        return $this->stripe;
    }

    public function setStripe(int $stripe): void
    {
        $this->stripe = $stripe;
    }

    public function getGoogleMaps(): int
    {
        return $this->googleMaps;
    }

    public function setGoogleMaps(int $googleMaps): void
    {
        $this->googleMaps = $googleMaps;
    }

    public function getOpenstreetmap(): int
    {
        return $this->openstreetmap;
    }

    public function setOpenstreetmap(int $openstreetmap): void
    {
        $this->openstreetmap = $openstreetmap;
    }

    public function getGoogleAds(): int
    {
        return $this->googleAds;
    }

    public function setGoogleAds(int $googleAds): void
    {
        $this->googleAds = $googleAds;
    }

    public function getVimeo(): int
    {
        return $this->vimeo;
    }

    public function setVimeo(int $vimeo): void
    {
        $this->vimeo = $vimeo;
    }

    /**
     * @param Essentials $essentialsField
     */
    public function addEssentialsField(Essentials $essentialsField): void
    {
        $this->essentialsFields->attach($essentialsField);
    }

    /**
     * @param Essentials $essentialsFieldToRemove The Essentials to be removed
     */
    public function removeEssentialsFields(Essentials $essentialsFieldToRemove): void
    {
        $this->essentialsFields->detach($essentialsFieldToRemove);
    }

    /**
     * @return ObjectStorage<Essentials> $essentialsFields
     */
    public function getEssentialsFields(): ObjectStorage
    {
        return $this->essentialsFields;
    }

    /**
     * @param ObjectStorage<Essentials> $essentialsFields
     */
    public function setEssentialsFields(ObjectStorage $essentialsFields): void
    {
        $this->essentialsFields = $essentialsFields;
    }

    public function addCustomField(Custom $customField): void
    {
        $this->customFields->attach($customField);
    }

    public function removeCustomField(Custom $customFieldToRemove): void
    {
        $this->customFields->detach($customFieldToRemove);
    }

    /**
     * @return ObjectStorage<Custom> $customFields
     */
    public function getCustomFields(): ObjectStorage
    {
        return $this->customFields;
    }

    /**
     * Sets the customFields
     *
     * @param ObjectStorage<Custom> $customFields
     */
    public function setCustomFields(ObjectStorage $customFields): void
    {
        $this->customFields = $customFields;
    }

    public function setMarketingFields(): void
    {
        $this->marketingFields = array_filter([
            'googleAnalytics' => $this->getGoogleAnalytics(),
            'googleTagManager' => $this->getGoogleTagManager(),
            'googleAds' => $this->getGoogleAds(),
            'googleAdsRemarketing' => $this->getGoogleAdsRemarketing(),
            'matomo' => $this->getMatomo(),
            'plausible' => $this->getPlausible() // Because Plausible does not need any Consent!
        ]);
    }

    public function getMarketingFields(): array
    {
        return $this->marketingFields;
    }

    public function setOthersFields(): void
    {
        $this->othersFields = array_filter([
            'googleMaps' => $this->getGoogleMaps(),
            'openstreetmap' => $this->getOpenstreetmap(),
            'stripe' => $this->getStripe(),
        ]);
    }

    public function getOthersFields(): array
    {
        return $this->othersFields;
    }

    public function setMediaFields(): void
    {
        $this->mediaFields = array_filter([
            'youtube' => $this->getYoutube(),
            'vimeo' => $this->getVimeo()
        ]);
    }

    public function getMediaFields(): array
    {
        return $this->mediaFields;
    }
}
