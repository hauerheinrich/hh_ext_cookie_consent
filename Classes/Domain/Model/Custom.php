<?php
namespace HauerHeinrich\HhExtCookieConsent\Domain\Model;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \TYPO3\CMS\Extbase\Domain\Model\FileReference;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\CookieItemInterface;

/***
 *
 * This file is part of the "hh_ext_cookie_consent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/
/**
 * Custom
 */
class Custom extends AbstractEntity implements CookieItemInterface {

    protected string $title = '';
    protected string $supplier = '';
    protected string $purpose = '';

    /**
     * @var FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $placeholderImage = NULL;

    protected string $cookieId = '';
    protected string $cookieName = '';
    protected string $cookieRuntime = '';
    protected string $cookiePrivacyLink = '';
    protected string $cspDefaultSrc = '';
    protected string $cspScriptSrc = '';
    protected string $cspStyleSrc = '';
    protected string $cspImgSrc = '';
    protected string $cspFontSrc = '';
    protected string $cspObjectSrc = '';
    protected string $cspMediaSrc = '';
    protected string $cspFormAction = '';
    protected string $cspFrameSrc = '';
    protected string $cspFrameAncestors = '';

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getSupplier(): string
    {
        return $this->supplier;
    }

    public function setSupplier(string $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getPurpose(): string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): void
    {
        $this->purpose = $purpose;
    }

    public function getPlaceholderImage(): FileReference|null
    {
        return $this->placeholderImage;
    }

    public function setPlaceholderImage(FileReference $placeholderImage): void
    {
        $this->placeholderImage = $placeholderImage;
    }

    public function getCookieId(): string
    {
        return $this->cookieId;
    }

    public function setCookieId(string $cookieId)
    {
        $this->cookieId = $cookieId;
    }

    public function getCookieName(): string
    {
        return $this->cookieName;
    }

    public function setCookieName(string $cookieName): void
    {
        $this->cookieName = $cookieName;
    }

    public function getCookieNames(): array
    {
        return explode(',', $this->getCookieName());
    }

    public function getCookieRuntime(): string
    {
        return $this->cookieRuntime;
    }

    public function setCookieRuntime(string $cookieRuntime): void
    {
        $this->cookieRuntime = $cookieRuntime;
    }

    public function getCookieRuntimes(): array
    {
        return explode(',', $this->getCookieRuntime());
    }

    public function getCookiePrivacyLink(): string
    {
        return $this->cookiePrivacyLink;
    }

    public function setCookiePrivacyLink(string $cookiePrivacyLink): void
    {
        $this->cookiePrivacyLink = $cookiePrivacyLink;
    }

    public function getCspDefaultSrc(): string
    {
        return $this->cspDefaultSrc;
    }

    public function setCspDefaultSrc(string $cspDefaultSrc): void
    {
        $this->cspDefaultSrc = $cspDefaultSrc;
    }

    public function getCspScriptSrc(): string
    {
        return $this->cspScriptSrc;
    }

    public function setCspScriptSrc(string $cspScriptSrc): void
    {
        $this->cspScriptSrc = $cspScriptSrc;
    }

    public function getCspStyleSrc(): string
    {
        return $this->cspStyleSrc;
    }

    public function setCspStyleSrc(string $cspStyleSrc): void
    {
        $this->cspStyleSrc = $cspStyleSrc;
    }

    public function getCspImgSrc(): string
    {
        return $this->cspImgSrc;
    }

    public function setCspImgSrc(string $cspImgSrc): void
    {
        $this->cspImgSrc = $cspImgSrc;
    }

    public function getCspFontSrc(): string
    {
        return $this->cspFontSrc;
    }

    public function setCspFontSrc(string $cspFontSrc): void
    {
        $this->cspFontSrc = $cspFontSrc;
    }

    public function getCspObjectSrc(): string
    {
        return $this->cspObjectSrc;
    }

    public function setCspObjectSrc(string $cspObjectSrc): void
    {
        $this->cspObjectSrc = $cspObjectSrc;
    }

    public function getCspMediaSrc(): string
    {
        return $this->cspMediaSrc;
    }

    public function setCspMediaSrc(string $cspMediaSrc): void
    {
        $this->cspMediaSrc = $cspMediaSrc;
    }

    public function getCspFormAction(): string
    {
        return $this->cspFormAction;
    }

    public function setCspFormAction(string $cspFormAction): void
    {
        $this->cspFormAction = $cspFormAction;
    }

    public function getCspFrameSrc(): string
    {
        return $this->cspFrameSrc;
    }

    public function setCspFrameSrc(string $cspFrameSrc): void
    {
        $this->cspFrameSrc = $cspFrameSrc;
    }

    public function getCspFrameAncestors(): string
    {
        return $this->cspFrameAncestors;
    }

    public function setCspFrameAncestors(string $cspFrameAncestors): void
    {
        $this->cspFrameAncestors = $cspFrameAncestors;
    }
}
