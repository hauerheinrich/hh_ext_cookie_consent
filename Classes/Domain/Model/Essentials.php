<?php
namespace HauerHeinrich\HhExtCookieConsent\Domain\Model;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\CookieItemInterface;

/***
 *
 * This file is part of the "hh_ext_cookie_consent" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/
/**
 * Essentials
 */
class Essentials extends AbstractEntity implements CookieItemInterface{

    protected string $title = '';
    protected string $supplier = '';
    protected string $purpose = '';
    protected string $cookieId = '';
    protected string $cookieName = '';
    protected string $cookieRuntime = '';
    protected string $cspDefaultSrc = '';
    protected string $cookiePrivacyLink = '';

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getSupplier(): string
    {
        return $this->supplier;
    }

    public function setSupplier(string $supplier): void
    {
        $this->supplier = $supplier;
    }

    public function getPurpose(): string
    {
        return $this->purpose;
    }

    public function setPurpose(string $purpose): void
    {
        $this->purpose = $purpose;
    }

    public function getCookieId(): string
    {
        return $this->cookieId;
    }

    public function setCookieId(string $cookieId): void
    {
        $this->cookieId = $cookieId;
    }

    public function getCookieName(): string
    {
        return $this->cookieName;
    }

    public function setCookieName(string $cookieName): void
    {
        $this->cookieName = $cookieName;
    }

    public function getCookieRuntime(): string
    {
        return $this->cookieRuntime;
    }

    public function setCookieRuntime(string $cookieRuntime): void
    {
        $this->cookieRuntime = $cookieRuntime;
    }

    public function getCookiePrivacyLink(): string
    {
        return $this->cookiePrivacyLink;
    }

    public function setCookiePrivacyLink(string $cookiePrivacyLink): void
    {
        $this->cookiePrivacyLink = $cookiePrivacyLink;
    }

    public function getCspDefaultSrc(): string
    {
        return $this->cspDefaultSrc;
    }

    public function setCspDefaultSrc(string $cspDefaultSrc): void
    {
        $this->cspDefaultSrc = $cspDefaultSrc;
    }
}
