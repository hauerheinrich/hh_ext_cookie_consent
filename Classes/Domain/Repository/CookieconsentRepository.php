<?php
namespace HauerHeinrich\HhExtCookieConsent\Domain\Repository;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Extbase\Persistence\Repository;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Extbase\Persistence\QueryInterface;
use \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

/***
 *
 * This file is part of the "Cookie Consent Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Christian Hackl <chackl@hauer-heinrich.de>, Hauer Heinrich
 *           Sebastian Pieczona <spieczona@hauer-heinrich.de>, Hauer-Heinrich
 *
 ***/
/**
 * The repository for Cures
 */
class CookieconsentRepository extends Repository {

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject(): void {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings */
        $querySettings = GeneralUtility::makeInstance(Typo3QuerySettings::class);

        $querySettings->setRespectStoragePage(FALSE);
        $querySettings->setRespectSysLanguage(TRUE);
        $querySettings->setIgnoreEnableFields(TRUE);
        $querySettings->setEnableFieldsToBeIgnored(['hidden']);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * findByPid
     *
     * @param integer $pid
     * @return Tx_Extbase_Persistence_QueryResult
     */
    public function findByPid(int $pid) {
        $query = $this->createQuery();
        $query->matching( $query->equals('pid', $pid) );

        return $query->execute();
    }
}
