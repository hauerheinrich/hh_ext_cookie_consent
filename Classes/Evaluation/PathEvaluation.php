<?php
declare(strict_types=1);

namespace HauerHeinrich\HhExtCookieConsent\Evaluation;

// use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Messaging\FlashMessageService;
use \TYPO3\CMS\Core\Messaging\FlashMessage;
use \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity;

/**
 * Class for field value validation/evaluation to be used in 'eval' of TCA
 * usage for example at field: cookie_css_path
 */
class PathEvaluation {
    /**
     * Server-side validation/evaluation on saving the record
     *
     * @param string $value The field value to be evaluated
     * @param string $is_in The "is_in" value of the field configuration from TCA
     * @param bool $set Boolean defining if the value is written to the database or not.
     * @return string Evaluated field value
     */
    public function evaluateFieldValue(string $value, string $is_in, bool &$set): string {
        $value = $this->deleteFirstChar($value);
        // check if file exists
        $path = $value;
        $this->checkFileExists($_SERVER['DOCUMENT_ROOT'].'/'.$this->replaceStringExt($path));

        return $value;
    }

    /**
     * Server-side validation/evaluation on opening the record
     *
     * @param array $parameters Array with key 'value' containing the field value from the database
     * @return string Evaluated field value
     */
    public function deevaluateFieldValue(array $parameters): string {
        $value = $this->deleteFirstChar($parameters['value']);

        return $value;
    }

    /**
     * checks if the given path / file exists
     * if not returns flash message
     */
    public function checkFileExists(string $filePath): bool {
        if(!file_exists($filePath)) {
            $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
            $messageQueue = $flashMessageService->getMessageQueueByIdentifier();

            $message = GeneralUtility::makeInstance(FlashMessage::class,
                'It seems that your css file doesn\'t exist!',
                'CSS file path', // [optional] the header
                ContextualFeedbackSeverity::WARNING,
                false
            );

            $messageQueue->addMessage($message);

            return false;
        }

        return true;
    }

    /**
     * delete first character if it is "/"
     */
    public function deleteFirstChar(string &$string, string $char = '/'): string {
        return ltrim($string, $char);
    }

    /**
     * replace "EXT:" with "typo3conf/ext/" in given string
     */
    public function replaceStringExt(string &$path): string {
        if (strpos($path, 'EXT:') === 0) {
            return substr_replace($path, 'typo3conf/ext/', 0, 4);
        }

        return $path;
    }
}
