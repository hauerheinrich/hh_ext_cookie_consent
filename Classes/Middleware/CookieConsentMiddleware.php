<?php
declare(strict_types=1);
namespace HauerHeinrich\HhExtCookieConsent\Middleware;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Server\MiddlewareInterface;
use \Psr\Http\Server\RequestHandlerInterface;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Database\ConnectionPool;
use \TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use \TYPO3\CMS\Core\Database\Connection;

class CookieConsentMiddleware implements MiddlewareInterface {

    public int $rootPageId = 1;
    public int $languageUid = 0;

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $response = $handler->handle($request);
        $this->rootPageId = $request->getAttribute('site')->getRootPageId();

        $context = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class);
        $this->languageUid = $context->getPropertyFromAspect('language', 'id');

        $post = $request->getParsedBody();
        $postData = [];
        if(!empty($post) && is_array($post)) {
            $postData = array_key_exists('tx_hhextcookieconsent_coco', $post) ? $post['tx_hhextcookieconsent_coco'] : [];
        }

        if(!empty($_COOKIE['hh_ext_cookie_consent']) && is_array($postData) && array_key_exists('modifyCookie', $postData)) {
            $cspStatesByPostOrCookie = $postData['modifyCookie'];
        } else {
            if (array_key_exists('hh_ext_cookie_consent', $_COOKIE)) {
                $cspStatesByPostOrCookie = json_decode($_COOKIE['hh_ext_cookie_consent'], true);
            } else {
                $cspStatesByPostOrCookie = false;
            }
        }

        $cspRule = '';
        $cspMode = '';
        $cookieRuleSet = [];
        $isSSL = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? true : false;

        // Set Cookie Variables
        $cookieName = 'hh_ext_cookie_consent';
        $cookieTime = time() + (86400 * 30);
        $cookiePath = '/';
        $cookieDomain = '';
        $cookieSecure = $isSSL ? true : false;

        if (isset($postData['submit_all'])) {
            $cspMode = 'all';
        } elseif (isset($postData['submit_individual'])) {
            $cspMode = 'individual';
        } elseif (isset($postData['submit_essentials'])) {
            $cspMode = 'essentials';
        }

        // Essential Cookies
        // (please also check the cookie-consent.js when essential cookies are set)
        if ($cspMode === 'essentials') {
            $this->setcookieSameSite($cookieName, json_encode(['essentials' => '1']), $cookieTime, $cookiePath, $cookieDomain, $cookieSecure);
        }
        // All & Individual/Custom Cookies
        elseif ($cspMode === 'individual' || $cspMode === 'all' || !empty($cspStatesByPostOrCookie)) {
            // CSP Rule Sets:
            $cspRuleSets = [];
            $cspRuleSetsCollection = [
                'googleAnalytics' => 'google-analytics.com *.google-analytics.com *.analytics.google.com',
                'googleTagManager' => 'googletagmanager.com *.googletagmanager.com googletagservices.com *.googletagservices.com',
                'googleAds' => '*.doubleclick.net googleadservices.com *.googleadservices.com www.google.com www.google.de www.google.fr www.google.it www.google.es www.google.pt www.google.uk www.google.pl www.google.nl admob.com *.admob.com adwords.com *.adwords.com googleapis.com *.googleapis.com googletraveladservices.com *.googletraveladservices.com',
                // TODO: googleAdsRemarketing seems obsolete?! (maybe just reasonable for backward compatibility):
                'googleAdsRemarketing' => 'googleadservices.com *.www.googleadservices.com googleads.g.doubleclick.net bid.g.doubleclick.net',
                'googleMaps' => 'www.google.com/maps maps.googleapis.com maps.google.com ajax.googleapis.com maps.gstatic.com',
                'openstreetmap' => 'openstreetmap.org *.openstreetmap.org openstreetmap.de *.openstreetmap.de',
                'youtube' => 'youtube.com *.youtube.com *.ytimg.com www.youtube-nocookie.com',
                'stripe' => 'stripe.com *.stripe.com',
                'vimeo' => 'vimeo.com *.vimeo.com *.vimeocdn.com',
                'matomo' => $this->getMatomoUrl()
                // TODO: Inject essential custom field's default_src here
            ];

            $beSettings = $this->getCookieConsentSettings();
            if($beSettings !== []) {
                foreach ($beSettings as $key => $value) {
                    if($value === 1) {
                        $key = lcfirst(GeneralUtility::underscoredToUpperCamelCase($key));
                        if(isset($cspRuleSetsCollection[$key])) {
                            $cspRuleSets[$key] = $cspRuleSetsCollection[$key];
                        }
                    }
                }
            }

            // Add custom fields to the cspRuleSets
            $customs = $this->getCurrentCustomsDomainModelObject();
            if(!empty($customs)) {
                foreach ($customs as $key => $value) {
                    if(!empty($value['csp_default_src'])) {
                        $cspRuleSets['custom'][$value['cookie_id']] = $value['csp_default_src'];
                    }
                }
            }

            // Build CSP Rules together (Marketing & Media)
            foreach($cspRuleSets as $key => $val) {
                if((isset($cspStatesByPostOrCookie[$key]) && $cspStatesByPostOrCookie[$key] == '1') || $cspMode == 'all') {
                    if(is_array($val)) {
                        foreach($val as $k => $v) {
                            $cspRule .= ' ' . trim($v);
                            $cookieRuleSet['custom'][$k] = '1';
                        }
                    } else {
                        $cspRule .= ' ' . trim($val);
                        $cookieRuleSet[$key] = '1';
                    }
                }
            }

            // Build CSP Rules together by POST INPUT
            if (!empty($postData['modifyCookie']['custom'])) {
                foreach($postData['modifyCookie']['custom'] as $key => $val) {
                    if($val == '1') {
                        // $cspRule .= " " . trim($val);
                        if(!empty($postData['modifyCookie']['custom']['domain'][$key])) {
                            $cspRule .= ' ' . trim($postData['modifyCookie']['custom']['domain'][$key]);
                        }
                        $cookieRuleSet['custom'][$key] = '1';
                    }
                }
            }
            // Build CSP Rules together by COOKIE INPUT
            else if(!empty($cspStatesByPostOrCookie['custom'])) {
                foreach($cspStatesByPostOrCookie['custom'] as $key => $val) {
                    $customEntries = $this->getCustomEntriesByCookieId((string) $key);
                    if(!empty($customEntries)) {
                        if(isset($customEntries['csp_default_src'])) {
                            $cspRule .= ' ' . trim($customEntries['csp_default_src']);
                        }

                        // TODO: else Log Message
                    }
                }
            }

            // Set Cookie with Cookie Rules
            if (!empty($postData)) {
                $cookieRule = json_encode($cookieRuleSet);
                $this->setcookieSameSite($cookieName, $cookieRule, $cookieTime, $cookiePath, $cookieDomain, $cookieSecure);
            }
        }

        // Essentials do not need the Consent of the User
        $essentials = $this->getCurrentEssentialsDomainModelObject();
        if(!empty($essentials)) {
            foreach ($essentials as $key => $value) {
                if(!empty($value['csp_default_src'])) {
                    $cspRule .= ' ' . trim($value['csp_default_src']);
                }
            }
        }

        // Plausible does not need the Consent of the User
        // (if Plausible is enabled, then Plausible is always implemented in the CSP Rules, because Plausible does not need any User Cookie or Consent)
        $cspRulePlausible = $this->getPlausible() === 1 ? ' plausible.io *.plausible.io ' . trim($this->getPlausibleUrl()) : "";
        $cspRule .= $cspRulePlausible;

        // Set CSP Header
        header_remove('Content-Security-Policy');
        // if($response->hasHeader('Content-Security-Policy')) { // DUPLICATE/REDUNDANT
        //     $response = $response->withHeader("Content-Security-Policy", "default-src 'self' {$cspRule} 'unsafe-inline' 'unsafe-eval' data:;");
        //     return $response;
        // }
        //
        // Old Method: header("Content-Security-Policy: default-src 'self' {$cspRule} 'unsafe-inline' 'unsafe-eval' data:;");
        $response = $response->withHeader("Content-Security-Policy", "default-src 'self' {$cspRule} 'unsafe-inline' 'unsafe-eval' data:;");
        return $response;
    }

    /**
     * getCustomEntriesByUid
     *
     * @param  int $uid
     * @return array
     */
    public function getCustomEntriesByUid(int $uid): array {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_custom');
        $customs = $queryBuilder
            ->select('uid', 'pid', 'cookie_id', 'title', 'supplier', 'purpose', 'csp_default_src')
            ->from('tx_hhextcookieconsent_domain_model_custom')
            ->where(
                $queryBuilder->expr()->eq('uid', $uid)
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        if (end($customs)) {
            return end($customs);
        }

        return [];
    }

    /**
     * getCustomEntriesByUid
     */
    public function getCustomEntriesByCookieId(string $cookieId): array {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_custom');
        $customs = $queryBuilder
            ->select('uid', 'pid', 'cookie_id', 'title', 'supplier', 'purpose', 'csp_default_src')
            ->from('tx_hhextcookieconsent_domain_model_custom')
            ->where(
                $queryBuilder->expr()->eq('cookie_id', $queryBuilder->createNamedParameter($cookieId, Connection::PARAM_STR))
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        if (end($customs)) {
            return end($customs);
        }

        return [];
    }

    /**
     * getCookieConsentSettings
     * from backend input module settings
     */
    public function getCookieConsentSettings(): array {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $rows = $queryBuilder
            ->select('*')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        return empty($rows[0]) ? [] : $rows[0];
    }

    /**
     * Returns the currentEssentialsDomainModel from current rootPage
     */
    public function getCurrentEssentialsDomainModelObject(): array {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $queryBuilder->getRestrictions()
            ->removeByType(HiddenRestriction::class);
        $rows = $queryBuilder
            ->select('uid')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        if ($rows) {
            $rows = $rows[0];

            // Essential Select Fields
            $queryBuilder2 = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_essentials');
            $customs = $queryBuilder2
                ->select('*')
                ->from('tx_hhextcookieconsent_domain_model_essentials')
                ->where(
                    $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                    $queryBuilder2->expr()->eq('cookieconsent', intval($rows['uid'])),
                    $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
                )
                ->executeQuery()
                ->fetchAllAssociative();

            return $customs;
        }

        return [];
    }

    /**
     * Returns the currentCustomsDomainModel from current rootPage
     */
    public function getCurrentCustomsDomainModelObject(): array {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $rows = $queryBuilder
            ->select('uid')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        if ($rows) {
            $rows = $rows[0];

            // Custom Select Fields
            $queryBuilder2 = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_custom');
            $customs = $queryBuilder2
                ->select('uid', 'pid', 'cookie_id', 'title', 'supplier', 'purpose', 'csp_default_src')
                ->from('tx_hhextcookieconsent_domain_model_custom')
                ->where(
                    $queryBuilder2->expr()->eq('cookieconsent', intval($rows['uid']))
                )
                ->executeQuery()
                ->fetchAllAssociative();

            return $customs;
        }

        return [];
    }

    /**
     * Returns the getMatomoUrl from current rootPage
     */
    public function getMatomoUrl(): string {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $rows = $queryBuilder
            ->select('matomo_url')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        return empty($rows[0]['matomo_url']) ? '' : $rows[0]['matomo_url'];
    }

    /**
     * Returns the getPlausible from current rootPage
     */
    public function getPlausible(): int {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $rows = $queryBuilder
            ->select('plausible')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        return isset($rows[0]['plausible']) ? $rows[0]['plausible'] : 0;
    }

    /**
     * Returns the getPlausibleUrl from current rootPage
     */
    public function getPlausibleUrl(): string {
        $queryBuilder = $this->getConnectionPool()->getQueryBuilderForTable('tx_hhextcookieconsent_domain_model_cookieconsent');
        $rows = $queryBuilder
            ->select('plausible_url')
            ->from('tx_hhextcookieconsent_domain_model_cookieconsent')
            ->where(
                $queryBuilder->expr()->eq('pid', intval($this->rootPageId)),
                $queryBuilder->expr()->eq('sys_language_uid', intval($this->languageUid)),
            )
            ->setMaxResults(1)
            ->executeQuery()
            ->fetchAllAssociative();

        return empty($rows[0]['plausible_url']) ? '' : $rows[0]['plausible_url'];
    }

    /**
     * getConnectionPool
     */
    public function getConnectionPool(): ConnectionPool {
        return GeneralUtility::makeInstance(ConnectionPool::class);
    }

    /**
     * Support samesite cookie flag in both php 7.2 (current production) and php >= 7.3 (when we get there)
     * From: https://github.com/GoogleChromeLabs/samesite-examples/blob/master/php.md and https://stackoverflow.com/a/46971326/2308553
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @param string $samesite
     * @return void
     */
    public function setcookieSameSite(string $name, string $value, int $expire = 0, string $path = '', string $domain = '', bool $secure = true, bool $httponly = false, string $samesite = 'Strict'): void {

        // https://web.dev/samesite-cookies-explained/?utm_source=devtools
        if (PHP_VERSION_ID < 70300) {
            setcookie($name, $value, $expire, "$path; samesite=$samesite", $domain, $secure, $httponly);
        } else {
            setcookie($name, $value, [
                'expires' => $expire,
                'path' => $path,
                'domain' => $domain,
                'samesite' => $samesite,
                'secure' => $secure,
                'httponly' => $httponly,
            ]);
        }
    }
}
