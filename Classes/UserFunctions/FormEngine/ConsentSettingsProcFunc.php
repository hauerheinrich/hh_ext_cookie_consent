<?php

namespace HauerHeinrich\HhExtCookieConsent\UserFunctions\FormEngine;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \HauerHeinrich\HhExtCookieConsent\Domain\Repository\CookieconsentRepository;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\Custom;
use \HauerHeinrich\HhExtCookieConsent\Domain\Model\Essentials;

/**
 * A user function
 */
class ConsentSettingsProcFunc {

    public function __construct(private CookieconsentRepository $cookieconsentRepository) {
    }

    public function getCookieUids(array &$params): void
    {
        $items = [];

        if(isset($params['site'])) {
            try {
                $siteRootPageId = $params['site']->getRootPageId();
                $consentSettings = $this->cookieconsentRepository->findByUid($siteRootPageId);

                if(!empty($consentSettings)) {
                    $consentSettingsEssentialsFields = $consentSettings->getEssentialsFields();
                    $consentSettingsCustomFields = $consentSettings->getCustomFields();

                    if(!empty($consentSettingsEssentialsFields)) {
                        $essentialsHeader = ['label' => 'Essentials', 'value' => '--div--'];
                        $itemsEssentials = [];
                        foreach ($consentSettingsEssentialsFields as $value) {
                            $itemsEssentials[] = $this->convertFieldToItem($value);
                        }

                        if(!empty($itemsEssentials)) {
                            $items[] = $essentialsHeader;
                            $items = array_merge($items, $itemsEssentials);
                        }
                    }

                    if(!empty($consentSettingsCustomFields)) {
                        $customHeader = ['label' => 'Custom', 'value' => '--div--'];
                        $itemsCustom = [];
                        foreach ($consentSettingsCustomFields as $value) {
                            $itemsCustom[] = $this->convertFieldToItem($value);
                        }

                        if(!empty($itemsCustom)) {
                            $items[] = $customHeader;
                            $items = array_merge($items, $itemsCustom);
                        }
                    }
                }

                $params['items'] = $items;
            } catch (\Throwable $th) {
                $params['items'][] = ['label' => 'No cookieId\'s set in the consent-module!', 'value' => 0];
            }
        }
    }

    protected function convertFieldToItem(Essentials|Custom $field): array {
        $result = ['label' => $field->getTitle(), 'value' => $field->getCookieId()];

        return $result;
    }
}
