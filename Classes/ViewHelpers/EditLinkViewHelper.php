<?php
namespace HauerHeinrich\HhExtCookieConsent\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use \TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use \TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Backend\Routing\UriBuilder;

/**
 * ViewHelper to create a link to edit a note
 *
 * @internal
 */
class EditLinkViewHelper extends AbstractViewHelper {
    use CompileWithRenderStatic;

    /**
     * Initializes the arguments
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument('uid', 'int', 'uid', true);
        $this->registerArgument('table', 'string', 'table ', true);
    }

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     *
     * @return string
     */
    public static function renderStatic(array $arguments, \Closure $renderChildrenClosure, RenderingContextInterface $renderingContext) {
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return (string)$uriBuilder->buildUriFromRoute(
            'record_edit',
            [
                'edit[' . $arguments['table'] . '][' . $arguments['uid'] . ']' => 'edit',
                'returnUrl' => GeneralUtility::getIndpEnv('REQUEST_URI')
            ]
        );
    }
}
