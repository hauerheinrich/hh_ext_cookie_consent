<?php
declare(strict_types=1);

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace HauerHeinrich\HhExtCookieConsent\ViewHelpers;

// use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use \TYPO3\CMS\Fluid\ViewHelpers\FormViewHelper AS OrigFormViewHelper;

/**
 * SPECIAL only for ConsentBanner usage!
 * Form ViewHelper. Generates a :html:`<form>` Tag. Tailored for extbase plugins, uses extbase Request.
 */
class FormViewHelper extends OrigFormViewHelper {

    /**
     * Sets the "action" attribute of the form tag
     */
    protected function setFormActionUri(): void {
        parent::setFormActionUri();
        $this->tag->addAttribute('action', $this->tag->getAttribute('action').$this->checkAgainstExcludedParameters($_GET));
    }

    /**
     * checkAgainstExcludedParameters()
     * returns string with allowed url parameters from $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters']!
     * e. g. ?utm_id=2&mtm_campaign=test
     *
     * @param array $GET
     * @return string
     */
    public function checkAgainstExcludedParameters(array $GET): string {
        $return = '';

        $cookieArray = [];
        if(!empty($_COOKIE['hh_ext_cookie_consent'])) {
            $cookieArray = \json_decode($_COOKIE['hh_ext_cookie_consent'], true);
        }

        if(!empty($GET) && !isset($cookieArray['cookieUrlParams']) && isset($GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters']) && !empty($GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'])) {
            $excludedParameters = $GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'];
            foreach ($GET as $key => $value) {
                if(\in_array($key, $excludedParameters)) {
                    $return .= '&' . $key . '=' . $value;
                }
            }

            if(!empty($return) && (empty($cookieArray) || !isset($cookieArray['cookieUrlParams']))) {
                $return = '?' . substr($return, 1);
                $cookieArray['cookieUrlParams'] = 1;

                $isSSL = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? true : false;
                $cookieName = 'hh_ext_cookie_consent';
                $cookieTime = time() + (86400 * 30);
                $cookiePath = '/';
                $cookieDomain = '';
                $cookieSecure = $isSSL ? true : false;
                $this->setcookieSameSite($cookieName, json_encode($cookieArray), $cookieTime, $cookiePath, $cookieDomain, $cookieSecure);
            }
        }

        return $return;
    }

    /**
     * Support samesite cookie flag in both php 7.2 (current production) and php >= 7.3 (when we get there)
     * From: https://github.com/GoogleChromeLabs/samesite-examples/blob/master/php.md and https://stackoverflow.com/a/46971326/2308553
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     * @param string $samesite
     * @return void
     */
    public function setcookieSameSite(string $name, string $value, int $expire = 0, string $path = '', string $domain = '', bool $secure = true, bool $httponly = false, string $samesite = 'Strict'): void {
        // https://web.dev/samesite-cookies-explained/?utm_source=devtools
        setcookie($name, $value, [
            'expires' => $expire,
            'path' => $path,
            'domain' => $domain,
            'samesite' => $samesite,
            'secure' => $secure,
            'httponly' => $httponly,
        ]);
    }
}
