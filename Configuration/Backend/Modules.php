<?php

use \HauerHeinrich\HhExtCookieConsent\Controller\Backend\CookieConsentModuleController;

/**
 * Definitions for modules provided by EXT:examples
 */
return [
    'cookieconsentmodule' => [
        'parent' => 'web',
        // 'position' => ['after' => 'web_info'],
        'access' => 'user,group',
        'workspaces' => 'live',
        // 'path' => '/module/cookieconsentmodule',
        'labels' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang.xlf',
        'icon'   => 'EXT:hh_ext_cookie_consent/Resources/Public/Icons/user_mod_extmodule.svg',
        'extensionName' => 'HhExtCookieConsent',
        // 'controllerActions' => [
        //     CookieConsentModuleController::class => [
        //         'index','cookieSelection','noPageSelected',
        //     ],
        // ],
        'routes' => [
            '_default' => [
                'target' => CookieConsentModuleController::class . '::indexAction',
            ],
            'cookieSelection' => [
                'target' => CookieConsentModuleController::class . '::cookieSelectionAction',
            ],
            'noPageSelected' => [
                'target' => CookieConsentModuleController::class . '::noPageSelectedAction',
            ]
        ],
    ],
];
