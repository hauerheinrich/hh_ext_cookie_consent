<?php
declare(strict_types = 1);

return [
    \HauerHeinrich\HhExtCookieConsent\Domain\Model\Cookieconsent::class => [
        'tableName' => 'tx_hhextcookieconsent_domain_model_cookieconsent',
        'properties' => [
            'crdate' => [
                'fieldName' => 'crdate'
            ],
            'tstamp' => [
                'fieldName' => 'tstamp'
            ],
        ],
    ],
];
