<?php
declare(strict_types=1);

return [
    'frontend' => [
        'hauerheinrich/frontend/hhextcookieconsent-cookie-consent' => [
            'target' => \HauerHeinrich\HhExtCookieConsent\Middleware\CookieConsentMiddleware::class,
            'description' => 'Cookie Consent Banner',
            'after' => [
                'typo3/cms-frontend/output-compression',
            ],
        ]
    ]
];
