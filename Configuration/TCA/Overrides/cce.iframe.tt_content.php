<?php
defined('TYPO3') || die();

call_user_func(function(string $extensionKey) {
    $tableName = 'tt_content';
    $pluginName = 'ConsentIframe';
    $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extensionKey));
    $pluginSignature = $extensionName . '_' . strtolower($pluginName);

    $GLOBALS['TCA']['tt_content']['columns']['consent_cce_link'] = [
        'exclude' => true,
        'label' => 'IFrame Url / Link',
        'description' => '',
        'config' => [
            'type' => 'link',
            'size' => 50,
            'appearance' => [
                'browserTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
            ],
        ],
    ];

    \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TCA'][$tableName], [
        'ctrl' => [
            'typeicon_classes' => [
                $pluginSignature => 'content-recent-news',
            ],
        ],
        'types' => [
            $pluginSignature => [
                'showitem' => '
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                        --palette--;;general,
                        --palette--;;headers,
                        pi_flexform,
                    --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,--palette--;;frames,--palette--;;appearanceLinks,,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,--palette--;;language,,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                      --palette--;;hidden,
                      --palette--;;access,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                         categories,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                         rowDescription,
                    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
                ',
            ],
        ],
    ]);

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
        $tableName,
        'CType',
        [
            'Consent iFrame',
            $pluginSignature,
            'content-recent-news',
        ],
        'textmedia',
        'after'
    );


    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        // extension name, matching the PHP namespaces (but without the vendor)
        $extensionKey,
        // arbitrary, but unique plugin name (not visible in the backend)
        $pluginName,
        // plugin title, as visible in the drop-down in the backend, use "LLL:" for localization
        'BannerConsent for iframes',
    );

    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,recursive,pages';
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        '*',
        'FILE:EXT:' . $extensionKey . '/Configuration/FlexForms/' . $pluginSignature . '.xml',
        $pluginSignature
    );

}, 'hh_ext_cookie_consent');
