<?php
defined('TYPO3') || die();

call_user_func(function(string $extensionKey) {

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'Hauer-Heinrich - Ext Cookie Consent'
    );
}, 'hh_ext_cookie_consent');
