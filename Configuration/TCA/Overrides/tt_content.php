<?php
defined('TYPO3') or die();

call_user_func(function(string $extensionKey) {
    $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extensionKey));

    // CookieConsent
    $pluginName = 'CoCo';
    $pluginSignature = $extensionName . '_' . strtolower($pluginName);

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        $extensionKey,
        $pluginName,
        'CookieConsent (CoCo)'
    );

    // $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,recursive,pages';
    // $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    // \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    //     $pluginSignature,
    //     'FILE:EXT:' . $extensionKey . '/Configuration/FlexForms/' . $pluginSignature . '.xml'
    // );
    // END: CookieConsent
}, 'hh_ext_cookie_consent');
