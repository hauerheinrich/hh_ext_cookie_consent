<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent',
        'label' => 'uid',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
        'hideTable' => 1,
        'searchFields' => 'title,',
        'iconfile' => 'EXT:hh_ext_cookie_consent/Resources/Public/Icons/tx_hhextcookieconsent_domain_model_cookieconsent.gif'
    ],
    'palettes' => [
        'language' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource',
        ],
        'media' => [
            'showitem' => 'youtube, vimeo'
        ],
        'services' => [
            'showitem' => 'google_maps, openstreetmap, --linebreak--,stripe'
        ],
        'matomo' => [
            'showitem' => 'matomo, matomo_url'
        ],
        'plausible' => [
            'showitem' => 'plausible, plausible_url'
        ],
        'cookie_essentials' => [
            'showitem' => 'cookie_essentials_header, --linebreak--, cookie_essentials_description'
        ],
    ],
    'types' => [
        '1' => [
            // google_ads_remarketing has been removed (seems to exists only for backward compatbility) / obsolete due to "google_ads"
            'showitem' => '
                --div--;Default Cookie / Settings,
                    cookie_vendor,
                    cookie_privacy_link,
                    cookie_imprint_link,
                    privacy_respect_gpc,
                    --palette--;;cookie_essentials,
                --div--;Layout,
                    cookie_overlay_settings_button,
                    cookie_footer_settings_button,
                    cookie_banner_close_icon,
                    cookie_banner_btn_essential_show,
                    cookie_theme_color,
                    cookie_theme,
                    cookie_css_path,
                --div--;Essentials,
                    essentials_fields,
                --div--;Marketing,
                    google_analytics,
                    google_tag_manager,
                    google_ads,
                    --palette--;;matomo,
                    --palette--;;plausible,
                --div--;Media & Services,
                    --palette--;Media;media,
                    --palette--;Services;services,
                --div--;Custom,
                    custom_fields,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                    hidden,
                    starttime,
                    endtime,
                --div--;Language,
                    --palette--;;language,
            '
        ],
    ],
    'columns' => [
        'pid' => [
            'label' => 'pid',
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ],
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language'
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hhextcookieconsent_domain_model_cookieconsent',
                'foreign_table_where' => 'AND {#tx_hhextcookieconsent_domain_model_cookieconsent}.{#pid}=###CURRENT_PID### AND {#tx_hhextcookieconsent_domain_model_cookieconsent}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        'label' => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        // 'cookie_name' => [
        //     'exclude' => true,
        //     'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_name.label',
        //     'config' => [
        //         'type' => 'input',
        //         'size' => 30,
        //         'eval' => 'trim'
        //     ],
        //     'default' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_name.default'
        // ],
        'cookie_vendor' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_vendor.label',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
            'default' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_vendor.default'
        ],
        'cookie_privacy_link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_privacy_link.label',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_privacy_link.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'blindLinkOptions' => 'folder, file, mail'
                        ]
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_imprint_link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_imprint_link.label',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.cookie_imprint_link.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'blindLinkOptions' => 'folder, file, mail'
                        ]
                    ]
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_overlay_settings_button' => [
            'exclude' => true,
            'label' => 'Cookie Settings Button (Overlay)',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'cookie_overlay_settings_button', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
            'description' => 'Shows a overlay cookie button in the frontend, so the user can re-open the cookie consent settings again. Alternatively you can add this functionality to a html element of your choice by adding the css class "cookie-open" and "cookie-open-icon" to it.'
        ],
        'cookie_footer_settings_button' => [
            'exclude' => true,
            'label' => 'Cookie Settings Footer Button',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'cookie_footer_settings_button', '' ],
                ],
                'default' => 1,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
            'description' => 'Injects a cookie link/button into the frontend footer, so the user can re-open the cookie consent settings again. Alternatively you can add this functionality to a html element of your choice by adding the css class "cookie-open" to it.'
        ],
        'cookie_banner_close_icon' => [
            'exclude' => true,
            'label' => 'Show the "close-icon" (X) at the cookie-banner',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'cookie_banner_close_icon', '' ],
                ],
                'default' => 1,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
            'description' => ''
        ],
        'cookie_banner_btn_essential_show' => [
            'exclude' => true,
            'label' => 'Show button "accept only essentials"',
            'description' => '',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'cookie_banner_btn_essential_show', '' ],
                ],
                'default' => 1,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'privacy_respect_gpc' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:privacy_respect_gpc.label',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:privacy_respect_gpc.description',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [ 'privacy_respect_gpc', '' ],
                ],
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'cookie_theme_color' => [
            'exclude' => true,
            'label' => 'Primary Theme Color (Hex Color) of the Cookie Plugin',
            'config' => [
                'type' => 'input',
                'renderType' => 'colorpicker',
                'size' => 30,
                //'min' => 6,
                //'max' => 6,
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
            'description' => 'You can change the primary theme color with this setting. Use a HEX Value only, e.g. #AABBCC'
            // 'default' => ''
        ],
        'cookie_theme' => [
            'label' => 'Cookie-theme selection',
            'description' => 'Sets css class to the cookie container, like: bottom-left, bottom-right...',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['Default', 'default'],
                    ['Bottom right', 'bottom-right'],
                    ['Bottom left', 'bottom-left'],
                    ['Bottom center', 'bottom-center'],
                    ['Center', 'center'],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_css_path' => [
            'label' => 'Path to your CSS-File',
            'description' => 'Overrides "cookie_theme" and "theme_color"! (without starting slash!)',
            'config' => [
                'type' => 'input',
                'default' => '',
                'placeholder' => 'e. g. fileadmin/user_upload/myCSS.min.css or EXT:your_ext_key/...',
                'eval' => 'trim, HauerHeinrich\\HhExtCookieConsent\\Evaluation\\PathEvaluation',
                'localizeReferencesAtParentLocalization' => true,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_essentials_header' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:cookie.essentialsHeader.label',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:cookie.essentialsHeader.description',
            'config' => [
                'type' => 'input',
                'default' =>' LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang.xlf:cookie.settings',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_essentials_description' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:cookie.essentialsDescription.label',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/TCA/locallang.xlf:cookie.essentialsDescription.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],

        'google_analytics' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.google_analytics',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'google_analytics', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'google_tag_manager' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.google_tag_manager',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'google_tag_manager', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'google_ads' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.google_ads',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'google_ads', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        // TODO: google_ads_remarketing has been removed (seems to exists only for backward compatbility) / obsolete due to "google_ads"
        'google_ads_remarketing' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.google_ads_remarketing',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'google_ads_remarketing', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'matomo' => [
            'exclude' => true,
            'onChange' => 'reload',
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.matomo',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.matomo.description',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'matomo', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'matomo_url' => [
            'exclude' => true,
            'displayCond' => 'FIELD:matomo:REQ:true',
            'label' => 'Custom Matomo Domain (for self-hosting)',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
                'placeholder' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'plausible' => [
            'exclude' => true,
            'onChange' => 'reload',
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.plausible',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'plausible', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'plausible_url' => [
            'exclude' => true,
            'displayCond' => 'FIELD:plausible:REQ:true',
            'label' => 'Custom Plausible Domain (for self-hosting)',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'max' => 255,
                'placeholder' => '',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'youtube' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.youtube',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'youtube', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'vimeo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.vimeo',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'vimeo', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'google_maps' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.google_maps',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'google_maps', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'openstreetmap' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.openstreetmap',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'openstreetmap', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'stripe' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.stripe',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ 'stripe', '' ],
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'essentials_fields' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.essentials_fields',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_hhextcookieconsent_domain_model_essentials',
                'foreign_field' => 'cookieconsent',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'custom_fields' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_cookieconsent.custom_fields',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_hhextcookieconsent_domain_model_custom',
                'foreign_field' => 'cookieconsent',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
            'description' => 'The custom fields will be displayed on the frontend in "Media & Services"'
        ],

    ],
];
