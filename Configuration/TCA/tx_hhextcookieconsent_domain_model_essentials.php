<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
        'hideTable' => 1,
        'searchFields' => 'title,supplier,purpose,cookie_name,cookie_privacy_link',
        'iconfile' => 'EXT:hh_ext_cookie_consent/Resources/Public/Icons/tx_hhextcookieconsent_domain_model_essentials.gif'
    ],
    'palettes' => [
        'language' => [
            'showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource',
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
                title,
                supplier,
                purpose,
                cookie_id,
                cookie_name,
                cookie_runtime,
                cookie_privacy_link,
                --div--;Domains,
                    csp_default_src,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
                    hidden,
                    starttime,
                    endtime,
                --div--;Language,
                    --palette--;;language,
            '],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language'
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hhextcookieconsent_domain_model_essentials',
                'foreign_table_where' => 'AND {#tx_hhextcookieconsent_domain_model_essentials}.{#pid}=###CURRENT_PID### AND {#tx_hhextcookieconsent_domain_model_essentials}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.title',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.title.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'supplier' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.supplier',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.supplier.description',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'purpose' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.purpose',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.purpose.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ],
        ],
        'cookie_id' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_id',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_id.description',
            'l10n_display' => 'defaultAsReadonly',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim,required,unique,alphanum'
            ],

        ],
        'cookie_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_name',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_name.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_runtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_runtime',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_runtime.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'cookie_privacy_link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_privacy_link',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_essentials.cookie_privacy_link.description',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
                'fieldControl' => [
                    'linkPopup' => [
                        'options' => [
                            'blindLinkOptions' => 'folder, file, mail'
                        ]
                    ]
                ]
            ],
        ],

        // TODO: missing csp_default_src for essentials domains for example chat tools
        'csp_default_src' => [
            'exclude' => true,
            'label' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_custom.csp_default_src',
            'description' => 'LLL:EXT:hh_ext_cookie_consent/Resources/Private/Language/locallang_db.xlf:tx_hhextcookieconsent_domain_model_custom.csp_default_src.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
    ],
];
