# Cookie Consent Banner

Usage (fluid): <f:cObject typoscriptObjectPath="lib.cookieConsent" />
Usage (backend): Go to the backend, Cookie Consent->Select Main Page

## Nice URLs (default)
Import the sites/config.yaml in your own sites/config.yaml, for example:
```
# ...
rootPageId: 1
base: 'https://www.domain.tld'
imports:
  - { resource: "EXT:hh_ext_cookie_consent/sites/config.yaml" }
routeEnhancers:
# ...
```

## Custom Placeholder
If you have custom elements with javascript which load dynamic elements from external sources, then use the following below (backend). The unique id can be found at Cookie Consent->Custom->ID (unique):

```
<div data-placeholder-id="uniqueid">
    Your code...
</div>
```

Or use the "Allowed URL's" from Cookie Consent->Custom->(tab)Domains->Allowed URL's:
```
<div data-placeholderfor="allowedUrls">
    Your code...
</div>
```

Or use with custom content
```
<div data-placeholderfor="[allowedUrls]"> <!-- (or with data-placeholder-id): -->
    Your code (e. g. iframe)...

    <div class="cc-placeholder-box">
        <h4 class="cookie-group-title cookie-group-type-custom cookie-group-title-[uniqueid]">Your custom header</h4>
        <a class="cc-placeholder-btn-accept">Your custom link text</a>
    </div>
</div>
```

IMPORTANT:
replace [uniqueid] with settings from backend "Cookie Consent module": "ID (unique)"[cookie_id] and [allowedUrls] with "Allowed URL's"[csp_default_src] !

## Custom open button
If you specify the optional data parameter "data-unique-id", the consent banner not only opens but also jumps to the relevant section.
For example, as an opt-out button on the data protection page for Google Analytics or Matomo or Plausible etc.
```
<button type="button" class="cookie-open" data-unique-id="[uniqueid]">[Your button text here]</button>
```

## Theming / Customization
You can add your own css file at the cookie-consent options (backend module).
If set, this option overwrites the css which is shipped with the extension!
The path shouldn't start with slash!
