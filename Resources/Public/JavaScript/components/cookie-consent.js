const cookieScriptLoaded = 1;

class PublishSubscribe {
    constructor() {
        this.events = {}
    }

    // on logic can be leveraged to fulfill subscribe
    subscribe(event, handler) {
        if(!this.events[event]) {
            this.events[event] = []
        }
        this.events[event].push(handler)
    }

    //off logic can be leveraged to fulfill unsubscribe
    unsubscribe(event, handler) {
        if(this.events[event]) {
            const index = this.events[event].findIndex(item => item === handler)
            this.events[event].splice(index, 1);
        }
    }

    //emit logic can be leveraged to fulfill unsubscribe
    publish(event, data) {
        if(this.events[event]) {
            this.events[event].forEach(handler => handler(data));
        }
    }
}


/**
 * Deep merge two or more objects or arrays.
 * (c) Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param   {*} ...objs  The arrays or objects to merge
 * @returns {*}          The merged arrays or objects
 */
function deepMerge(...objs) {
    /**
     * Get the object type
     * @param  {*}       obj The object
     * @return {String}      The object type
     */
    function getType(obj) {
        return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
    }

    /**
     * Deep merge two objects
     * @return {Object}
     */
    function mergeObj(clone, obj) {
        for (let [key, value] of Object.entries(obj)) {
            let type = getType(value);
            if (clone[key] !== undefined && getType(clone[key]) === type && ['array', 'object'].includes(type)) {
                clone[key] = deepMerge(clone[key], value);
            } else {
                clone[key] = structuredClone(value);
            }
        }
    }

    // Create a clone of the first item in the objs array
    let clone = structuredClone(objs.shift());

    // Loop through each item
    for (let obj of objs) {
        // Get the object type
        let type = getType(obj);

        // If the current item isn't the same type as the clone, replace it
        if (getType(clone) !== type) {
            clone = structuredClone(obj);
            continue;
        }

        // Otherwise, merge
        if (type === 'array') {
            clone = [...clone, ...structuredClone(obj)];
        } else if (type === 'object') {
            mergeObj(clone, obj);
        } else {
            clone = obj;
        }
    }

    return clone;
}

document.addEventListener("DOMContentLoaded", function() {
    // Variables - Elements
    let cookieTheme = document.querySelector(".cookie-consent-theme");
    let cookieOverlay = document.querySelector(".cookie-overlay");
    let cookieBoxContainer = document.querySelector(".cookie-consent-container");
    let cookieBox = document.querySelector(".cookie-consent-tool");
    let cookieBoxSettings = typeof cookieBox != "undefined" ? cookieBox.dataset : false;
    let cookieSettingsSimple = document.querySelector(".cookie-settings--simple");
    let cookieSettingsComplex = document.querySelector(".cookie-settings--complex");
    let cookieButtonOpen = document.querySelectorAll(".cookie-open");
    let cookieButtonClose = document.querySelectorAll(".cookie-close");
    let cookieModeToggler = document.querySelectorAll(".cookie-mode-toggler");
    // let cookieFooterSettingsButton = document.querySelector(".cookie-footer-settings-button") ? true : false;
    // let cookieSubmitEssentials = document.querySelector("input[type='submit'][name$='[submit_essentials]']");
    // let cookieSubmitIndividual = cookieSettingsComplex.querySelector("input[type='submit'][name$='[submit_individual]']");
    let cookieJson = getCookie("hh_ext_cookie_consent") ? JSON.parse(decodeURIComponent(getCookie("hh_ext_cookie_consent"))) : false;
    let acc = document.getElementsByClassName("cookie-group-tab");

    let langCountryCode = typeof document.querySelector("html").lang != "undefined" ? document.querySelector("html").lang : false;
    let langDict = {
        "de": {
            "privacy": "Datenschutz",
            "cookie_settings_edit": "Cookie Einstellungen ändern",
            "media_placeholder_desc": "Um dieses Medium betrachten zu können klicken Sie bitte auf den nachfolgenden Button um die Cookie & Datenschutzrichtlinien zu akzeptieren: ",
            "media_placeholder_btn": "Medium ansehen und Datenschutzerklärung dieses Anbieters akzeptieren",
            "time_series": ["Minuten", "Minute", "Stunden", "Stunde", "Tage", "Tag", "Wochen", "Woche", "Monate", "Monat", "Jahre", "Jahr"]
        },
        "en": {
            "privacy": "Privacy",
            "cookie_settings_edit": "Edit Cookie Settings",
            "media_placeholder_desc": "To view this medium please click on the button below to accept the cookie & privacy policy: ",
            "media_placeholder_btn": "Show media element and accept cookie & privacy policy",
            "time_series": ["minutes", "minute", "hours", "hour", "days", "day", "weeks", "week", "months", "month", "years", "year"]
        },
        "it": {
            "privacy": "Protezione dei dati",
            "cookie_settings_edit": "Modificare le impostazioni dei Cookies",
            "media_placeholder_desc": "Per visualizzare questo media, cliccare sul seguente pulsante per accettare l'Informativa sulla privacy e Cookies: ",
            "media_placeholder_btn": "Visualizza l'elemento mediatico e accetta Cookies e l'informativa sulla privacy",
            "time_series": ["minuti", "minuto", "ore", "ora", "giorni", "giorno", "settimane", "settimana", "mesi", "mese", "anni", "anno"]
        },
        "fr": {
            "privacy": "Protection des données",
            "cookie_settings_edit": "Modifier les paramètres des Cookies",
            "media_placeholder_desc": "Pour afficher ce média, veuillez cliquer sur le bouton suivant pour accepter les Cookies et la politique de confidentialité: ",
            "media_placeholder_btn": "Afficher l'élément média et accepter les Cookies et la politique de confidentialité",
            "time_series": ["minutes", "minute", "les heures", "heure", "jours", "jour", "semaines", "semaine", "mois", "mois", "années", "année"]
        },
        "es": {
            "privacy": "Protección de datos",
            "cookie_settings_edit": "Editar la configuración de las Cookies",
            "media_placeholder_desc": "Para ver este medio, por favor haga clic en el botón de abajo para aceptar Cookies y la política de privacidad: ",
            "media_placeholder_btn": "Ver el elemento multimedia y aceptar Cookies y la política de privacidad",
            "time_series": ["minutos", "minuto", "horas", "hora", "dias", "dia", "semanas", "semana", "meses", "mes", "años", "año"]
        },
        "pt": {
            "privacy": "Proteção de dados",
            "cookie_settings_edit": "Editar configurações de Cookies",
            "media_placeholder_desc": "Para visualizar esta mídia, clique no botão abaixo para aceitar Cookies & política de privacidade: ",
            "media_placeholder_btn": "Mostrar elemento de mídia e aceitar Cookies e política de privacidade",
            "time_series": ["minutos", "minuto", "horas", "hora", "dias", "dia", "semanas", "semana", "meses", "mes", "anos", "ano"]
        }
    };

    const pubSubClient = new PublishSubscribe();

    if(document.querySelector(".cookie-placeholder-translations")) {
        try {
            const overwrite = JSON.parse(document.querySelector(".cookie-placeholder-translations").innerHTML);
            langDict = deepMerge(langDict, overwrite);
        } catch (error) {
            debugMsg('Custom translation failed!', 'Failed on ".cookie-placeholder-translations" - JSON valid?');
        }
    }

    let langId = langCountryCode.substring(0, 2);
    let langDictValue = langDict[langCountryCode.substring(0, 2)] ? langDict[langCountryCode.substring(0, 2)] : langDict["en"];
    let lang = langCountryCode ? langDictValue : false; // Usage Example: lang["privacy"]

    // Multimedia Enabler - A placeholder will be shown if a media service wasn't enabled by the user
    // (the user can enable/accept media services/cookies (e.g. YouTube, GoogleMaps) on the website directly through a additional button)
    let media = {
        "maps": {
            "googleMaps": document.querySelectorAll("iframe[src^='https://www.google.com/maps'], .gmaps, #gmaps, .googlemaps, #googlemaps, #googleMap, #map_wrapper, .gmaps_map"),
            "openstreetmap": document.querySelectorAll("iframe[src^='https://www.openstreetmap.org/'], .leaflet-container:not(.openstreetmap), .openstreetmap, .osm-wrapper")
        },
        "video": {
            "youtube": document.querySelectorAll("iframe[src^='https://www.youtube-nocookie.com/'], iframe[src^='https://www.youtube.com/']"),
            "vimeo": document.querySelectorAll("iframe[src^='https://player.vimeo.com/']")
        },
        "custom": {

        }
    };

    // Helper Functions
    let isDebug = document.querySelector(".cookie-consent-theme") && (document.querySelector(".cookie-consent-theme").dataset.debug == "1" || document.URL.search("debug=true") != -1 || document.URL.search("dbug=true") != -1) ? true : false;

    function debugMsg(title, data) {
        if(isDebug) {
            console.log(title, data);
        }
    }

    // Custom placeholder content from CCE consentiframe
    if(cookieJson) {
        const consentTemplates = document.querySelectorAll("template.placeholder-template");
        if(consentTemplates) {
            consentTemplates.forEach(function(el) {
                const parentWrapper = el.parentNode;
                if(parentWrapper) {
                    const consentTemplateCookieId = parentWrapper.dataset.placeholderId;

                    if(consentTemplateCookieId) {
                        if(cookieJson['custom']) {
                            if(cookieJson['custom'][consentTemplateCookieId] != 1) {
                                const clon = el.content.cloneNode(true);
                                parentWrapper.classList.add("cc-placeholder");
                                parentWrapper.appendChild(clon);
                            }
                        }
                    }
                }
            });
        }
    }

    // Plausible
    // (Prevent user changes of plausible, because it does not need any User Consent/Cookies!)
    if(document.querySelector("input[id$=plausible]")){
        document.querySelectorAll("input[id$=plausible]").forEach(function(el){
            el.setAttribute("disabled", "");
        });
    }

    // Reuse/Add media logic to custom media elements (IFrame, etc.)
    document.querySelectorAll("input[type='checkbox'][name*='[custom]']").forEach(function(el) {
        if(el.dataset.cspCookieId && el.dataset.cspDefaultSrc) {
            // el.dataset.placeholderImage
            media["custom"][el.dataset.cspCookieId] = document.querySelectorAll("iframe[src^='https://"+el.dataset.cspDefaultSrc+"/'], audio[src^='https://"+el.dataset.cspDefaultSrc+"/'], embed[src^='https://"+el.dataset.cspDefaultSrc+"/'], img[src^='https://"+el.dataset.cspDefaultSrc+"/'], video[src^='https://"+el.dataset.cspDefaultSrc+"/'], form[action^='https://"+el.dataset.cspDefaultSrc+"/']");

            // Fallback - Data Custom Placeholder (see "Custom Placeholder" in Readme.md)
            // Usage: <div data-placeholder-id="uniqueid">Your Custom Code</div>
            if(media["custom"][el.dataset.cspCookieId].length == 0) {
                if(document.querySelectorAll("[data-placeholder-id='"+el.dataset.cspCookieId+"']").length){
                    media["custom"][el.dataset.cspCookieId] = document.querySelectorAll("[data-placeholder-id='"+el.dataset.cspCookieId+"']");
                } else if(document.querySelectorAll("[data-placeholderfor='"+el.dataset.cspDefaultSrc+"']").length) {
                    media["custom"][el.dataset.cspCookieId] = document.querySelectorAll("[data-placeholderfor='"+el.dataset.cspDefaultSrc+"']");
                }
            }
        } else {
            debugMsg("cookie consent [INFO]: cspDefaultSrc and/or cspUid is not set for the following element: ", el);
        }
    });

    // debugMsg("cookie consent [INFO]: cookieJson ", cookieJson);
    // debugMsg("cookie consent [INFO]: media ", media);

    // ScrollTo if Position has been set
    if(localStorage.getItem("hh_ext_cookie_consent_scrollpos")) {
        window.scrollTo(0, localStorage.getItem("hh_ext_cookie_consent_scrollpos"));
        localStorage.removeItem("hh_ext_cookie_consent_scrollpos");
    }

    // Show Cookie Box if no cookie is set
    if(cookieJson === false && cookieTheme.dataset.gpc == "1") {
        cookieBoxContainer.classList.add("cc-hide");
        const event = new CustomEvent("hide-ccb");
        cookieBoxContainer.dispatchEvent(event);
        pubSubClient.publish('hide-ccb');
    } else if(!getCookie("hh_ext_cookie_consent") || getCookie("hh_ext_cookie_consent") == null) {
        cookieBoxContainer.classList.remove("cc-hide");
        const event = new CustomEvent("hide-ccb");
        cookieBoxContainer.dispatchEvent(event);
        pubSubClient.publish('show-ccb');
    } else if(cookieJson.cookieUrlParams == 1) {
        cookieBoxContainer.classList.remove("cc-hide");
        const event = new CustomEvent("hide-ccb");
        cookieBoxContainer.dispatchEvent(event);
        pubSubClient.publish('show-ccb');
    } else {
        cookieBoxContainer.classList.add("cc-hide");
        const event = new CustomEvent("hide-ccb");
        cookieBoxContainer.dispatchEvent(event);
        pubSubClient.publish('hide-ccb');
    }

    // Translate List of timeSeries - Cookie Runtime
    document.querySelectorAll(".cookie-text-runtime span").forEach(function(el) {
        let text = el.innerText.toLowerCase();

        for(const lng in langDict) {
            let strFound = false;

            if(langId != lng) {
                langDict[lng]["time_series"].forEach(function(str, i) {
                    if(text.search(str) !== -1) {
                        text = text.replace(str, langDictValue["time_series"][i]);
                        strFound = true;
                    }
                });

                if(strFound) {
                    break;
                }
            }
        }

        el.innerText = text;
    });

    // Preselection of Input Fields
    for(let mediaCategoryKey in media) {
        for(let mediaKey in media[mediaCategoryKey]) {
            if(media[mediaCategoryKey][mediaKey]) {
                media[mediaCategoryKey][mediaKey].forEach(function(el) {
                    const cookiePreselect = document.querySelector("input[type='checkbox'][name$='["+mediaKey+"]']");

                    debugMsg("cookie consent [INFO]: cookiePreselect: ", cookiePreselect);

                    // Create Custom/Media Placeholder for easier User Activation
                    // (but only if the cookie JSON doesn't align with the media element given)
                    // (e.g. if youtube hasn't been activated by the user)
                    // if(cookiePreselect && !cookiePreselect.checked){
                    //
                    // TODO: The CookieHook still uses 2 formats, e.g. { "custom-5": "1" } and { "custom": { "5": "1" } }
                    // one of those needs to be removed / merged
                    // (the generation of those formats depend on "Accept all cookies" and "individual" cookies)
                    if(cookiePreselect && !cookieJson["custom-"+mediaKey] && !cookieJson[mediaKey] && (!cookieJson["custom"] || !cookieJson["custom"][mediaKey])) {
                        let placeholder = el.tagName == "IFRAME" ? el.parentNode : el;

                        placeholder.classList.add("cc-placeholder");
                        placeholder.classList.add("cc-placeholder-" + mediaCategoryKey);
                        placeholder.classList.add("cc-placeholder-" + mediaKey);

                        if(typeof cookieTheme.getAttribute("data-primary-color") != "undefined"){
                            placeholder.setAttribute("style", "--primaryColor:" + cookieTheme.getAttribute("data-primary-color"));
                            debugMsg("cookie consent [INFO]: Cookie Theme Color is set to ", cookieTheme.getAttribute("data-primary-color"));
                        }

                        if(cookiePreselect.dataset.placeholderImage) {
                            placeholder.style.backgroundImage = "url("+cookiePreselect.dataset.placeholderImage+")";
                        }

                        // check if custom placeholder-box is given
                        let placeholderBox = '',
                            placeholderBtnAccept = '';
                        if(placeholder.querySelector(".cc-placeholder-box")) {
                            placeholderBox = placeholder.querySelector(".cc-placeholder-box");
                            placeholderBtnAccept = placeholderBox.querySelector(".cc-placeholder-btn-accept");
                        } else {
                            placeholderBox = document.createElement("div"),
                            placeholderBtnAccept = document.createElement("a");
                            const placeholderTextHead = document.querySelector(".cookie-group-title-" + (cookiePreselect.dataset.cspCookieId ? cookiePreselect.dataset.cspCookieId : mediaKey));
                            // var placeholderTextBody = document.querySelector(".cookie-group-bodytext-" + (cookiePreselect.dataset.cspCookieId ? cookiePreselect.dataset.cspCookieId : mediaKey));

                            if(placeholderTextHead) {
                                placeholderBox.innerHTML = (placeholderTextHead ? placeholderTextHead.outerHTML : "");
                            } else {
                                debugMsg("cookie consent [WARNING]: No placeholderTextHead and/or placeholderTextBody defined for media key: ", mediaKey);
                                placeholderBox.innerHTML = "";
                            }

                            // placeholderBox.innerHTML += "<p class='cc-placeholder-desc'>"+ lang["media_placeholder_desc"] +"</p>";
                            placeholderBox.classList.add("cc-placeholder-box");
                            placeholderBtnAccept.innerText = lang["media_placeholder_btn"];
                            placeholderBtnAccept.classList.add("cc-placeholder-btn-accept");

                            placeholderBox.appendChild(placeholderBtnAccept);
                            placeholder.appendChild(placeholderBox);
                        }

                        placeholderBtnAccept.addEventListener("click", function(e) {
                            e.preventDefault();

                            if(cookiePreselect) {
                                cookiePreselect.checked = true;
                                cookiePreselect.value = 1;

                                localStorage.setItem("hh_ext_cookie_consent_scrollpos", parseInt(window.pageYOffset || document.documentElement.scrollTop));

                                cookieBoxContainer.classList.remove("cc-hide");
                                cookieSettingsSimple.classList.remove("cc-active");
                                cookieSettingsComplex.classList.add("cc-active");
                                const event = new CustomEvent("show-ccb");
                                cookieBoxContainer.dispatchEvent(event);
                                pubSubClient.publish('show-ccb');

                                // debugMsg(cookiePreselect.closest(".cookie-group").querySelector(".cookie-group-tab:not(.cc-active)"));

                                if (Element.prototype.closest) {
                                    if(cookiePreselect.closest(".cookie-group")) {
                                        if(cookiePreselect.closest(".cookie-group").querySelector(".cookie-group-tab:not(.cc-active)")) {
                                            cookiePreselect.closest(".cookie-group").querySelector(".cookie-group-tab:not(.cc-active)").click();
                                            const scrollToElement = document.querySelector(".cookie-consent-container .cookie-group-title-"+cookiePreselect.dataset.cspCookieId);

                                            setTimeout(() => {
                                                scrollToElement.scrollIntoView({block: "start", inline: "nearest", behavior: "smooth"});
                                            }, 1100);
                                        }

                                    } else {
                                        debugMsg("cookie consent [WARNING]: No cookie group (accordion) found for ", cookiePreselect);
                                    }
                                }

                                // cookieSettingsComplex.querySelector("form").submit();
                                // cookieSubmitIndividual.click();
                            } else {
                                debugMsg("cookie consent [WARNING]: This cookie input field does not exist: ", mediaKey);
                            }
                        });
                    }
                });
            }
        }
    }

    // Preselect Fields/Checkboxes of the cookie form depending on the csp cookie values
    if(cookieJson && typeof cookieJson["essentials"] == "undefined") {
        for(cookieKey in cookieJson) {
            if(cookieKey.match(/^[a-zA-Z0-9-_]+$/i)) {
                let inputPreselect = document.querySelector("input[type='checkbox'][name$='["+ cookieKey +"]']");

                if(cookieKey == "custom") {
                    for(cookieKeyCustom in cookieJson[cookieKey]) {
                        let inputPreselect = document.querySelector("input[type='checkbox'][name$='[custom]["+ cookieKeyCustom.replace("custom", "") +"]']");

                        if(inputPreselect) {
                            inputPreselect.checked = true;
                            inputPreselect.value = 1;
                        } else {
                            debugMsg("cookie consent [ERROR]: This cookie input field does not exist: ", cookieKeyCustom);
                        }
                    }
                } else if(inputPreselect) {
                    inputPreselect.checked = true;
                    inputPreselect.value = 1;
                } else {
                    debugMsg("cookie consent [ERROR]: This cookie input field does not exist: ", cookieKey);
                }
            } else {
                debugMsg("cookie consent [ERROR]: This cookieKey name has prohibited characters: ", cookieKey);
            }
        }
    }

    // debugMsg("cookie consent [INFO]: JSON ", cookieJson);

    // Individual Cookie Button & Page / Pagination
    cookieModeToggler.forEach(function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();

            if(cookieSettingsComplex.classList.contains("cc-active")){
                cookieSettingsSimple.classList.add("cc-active");
                cookieSettingsComplex.classList.remove("cc-active");
            } else {
                cookieSettingsSimple.classList.remove("cc-active");
                cookieSettingsComplex.classList.add("cc-active");
            }
        });
    });

    // Cookie Footer Settings Button (creates automatically a footer link for the user to reopen the cookie box)
    if(cookieBoxSettings.footerSettingsButton == "1") {
        const footerPrivacyProtectionLink = document.querySelectorAll(".footer li, #footer li"),
              privacyText = lang["privacy"] ? lang["privacy"] : false;
        let cookieButtonFooterEdit;

        if(footerPrivacyProtectionLink.length) {
            footerPrivacyProtectionLink.forEach(function(el) {
                const elLink = el.querySelector("a") ? el.querySelector("a") : false,
                      elInnerHTML = elLink ? elLink.innerHTML.toLowerCase() : false;

                if(elLink && lang && privacyText && elInnerHTML.search(privacyText.toLowerCase()) != -1 && typeof cookieButtonFooterEdit == "undefined") {
                    cookieButtonFooterEdit = document.createElement("li");
                    cookieButtonFooterEdit.classList.add("cookie-open-wrapper");
                    cookieButtonFooterEdit.innerHTML = "<a class='cookie-open' href='#'>"+lang["cookie_settings_edit"]+"</a>";
                    cookieButtonFooterEdit.addEventListener("click", function(e){
                        e.preventDefault();
                        cookieBoxContainer.classList.remove("cc-hide");
                        const event = new CustomEvent("show-ccb");
                        cookieBoxContainer.dispatchEvent(event);
                        pubSubClient.publish('show-ccb');
                    });

                    el.insertAdjacentElement("afterend", cookieButtonFooterEdit);
                }
            });
        }
    }

    // Create link(s) functionality for reopening the Cookie Box/Settings
    cookieButtonOpen.forEach(function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            cookieBoxContainer.classList.remove("cc-hide");
            const event = new CustomEvent("show-ccb");
            cookieBoxContainer.dispatchEvent(event);
            pubSubClient.publish('show-ccb');

            // open and jump to specified section of Accordion Menu
            const specifiedContentId = this.dataset.uniqueId;

            if(specifiedContentId) {
                const cookieGroupTitle = cookieSettingsComplex.querySelector(".cookie-group-title-" + specifiedContentId);
                const cookieGroupTitleClasses = cookieGroupTitle.classList;

                for (let classesI = 0; classesI < cookieGroupTitleClasses.length; classesI++) {
                    const element = cookieGroupTitleClasses[classesI];

                    if(element.startsWith("cookie-group-type-")) {
                        const groupId = element.split("cookie-group-type-").pop();
                        const cookieGroupContainer = cookieSettingsComplex.querySelector(".cookie-group-"+groupId);
                        console.log(cookieGroupContainer);

                        if(cookieGroupContainer) {
                            cookieSettingsSimple.classList.remove("cc-active");
                            cookieSettingsComplex.classList.add("cc-active");
                            const tab = cookieGroupContainer.querySelector(".cookie-group-tab");
                            const view = cookieGroupContainer.querySelector(".cookie-group-view");

                            tab.classList.add("cc-active");
                            view.classList.add("cc-active");
                            view.style.maxHeight = view.scrollHeight + "px";
                            setTimeout(() => {
                                cookieGroupTitle.scrollIntoView({behavior: 'smooth'});
                            }, 800);
                        }
                        break;
                    }
                }

                // if(cookieGroupTitle) {
                //     const cookieGroupContainer = cookieGroupTitle.parentElement.parentElement;

                //     if(cookieGroupContainer) {
                //         const tab = cookieGroupContainer.querySelector(".cookie-group-tab");
                //         const view = cookieGroupContainer.querySelector(".cookie-group-view");

                //         tab.classList.add("cc-active");
                //         view.classList.add("cc-active");
                //         view.style.maxHeight = view.scrollHeight + "px";
                //     }
                // }
            }
        });
    });

    // Close Button (optional)
    cookieButtonClose.forEach(function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            setCookieDefault();
        });
    });

    // Overlay Close Logic
    cookieOverlay.addEventListener("click", function(e) {
        e.preventDefault();
        setCookieDefault();
    });

    // Helper Functions
    function getCookie(name) {
        var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
        return v ? v[2] : null;
    }

    function setCookie(cname, cvalue, exdays) {
        let d = new Date();
        const expiryDays = typeof exdays != "undefined" ? exdays : 30;
        d.setTime(d.getTime() + (expiryDays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function setCookieDefault() {
        if(!getCookie("hh_ext_cookie_consent")) {
            setCookie("hh_ext_cookie_consent", '{"essentials":"1"}');
        }

        cookieBoxContainer.classList.add("cc-hide");
        const event = new CustomEvent("hide-ccb");
        cookieBoxContainer.dispatchEvent(event);
        pubSubClient.publish('hide-ccb');
    }
});
