<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hh_ext_cookie_consent"
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['hh_ext_cookie_consent'] = [
    'title' => 'Hauer-Heinrich - Cookie Consent Extension',
    'description' => 'Shows a consent-banner at the frontend - configurable at the backends own module.',
    'category' => 'plugin',
    'author' => 'Sebastian Pieczona, Christian Hackl',
    'author_email' => 'spieczona@hauer-heinrich.de, web@hauer-heinrich.de',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.12.4',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.2-12.4.99',
        ],
        'conflicts' => [
            'min' => ''
        ],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'HauerHeinrich\\HhExtCookieConsent\\' => 'Classes',
        ],
    ],
];
