<?php
defined('TYPO3') || die('Access denied.');

call_user_func(function() {
    $extensionKey = 'hh_ext_cookie_consent';

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        $extensionKey,
        'CoCo',
        [
            \HauerHeinrich\HhExtCookieConsent\Controller\ConsentBannerController::class => 'bannerOutput',
        ],
        // non-cacheable actions
        [
            \HauerHeinrich\HhExtCookieConsent\Controller\ConsentBannerController::class => 'bannerOutput',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        // extension name, matching the PHP namespaces (but without the vendor)
        $extensionKey,
        // arbitrary, but unique plugin name (not visible in the backend)
        'ConsentIframe',
        // all actions
        [
            \HauerHeinrich\HhExtCookieConsent\Controller\CceController::class => 'consentIframe',
        ],
        // non-cacheable actions
        [
            \HauerHeinrich\HhExtCookieConsent\Controller\CceController::class => ''
        ],
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );

    // Register the class to be available in 'eval' of TCA
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals']['HauerHeinrich\\HhExtCookieConsent\\Evaluation\\PathEvaluation'] = '';
});
